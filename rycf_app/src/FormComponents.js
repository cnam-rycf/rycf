import React, { useState } from 'react';
import { profils } from './config/temoins_config';
import { BarChart } from 'react-easy-chart';
import { render } from 'react-dom';
import axios from 'axios';

function FormComponent(props) {
  const eletro = ["Four", "Lave Vaiselle", "Lave Linge", "Sèche linge", "Refrigérateur", "Congélateur", "Micro-onde"];

  const electrique = ["Smartphone", "TV", "Tablette", "PC Portable", "PC Fixe", "Console", "Imprimante", "Box"];

  //Variable pour les transports
  const [vehiculeKm, setVehiculeKm] = useState(props.nbKm === undefined ? 0 : props.nbKm);
  const kmCalcul = (event) => {
    setVehiculeKm(event.target.value);
  };

  const [typeVehicule, setTypeVehicule] = useState(props.typeVehicule === undefined ? 'essence' : props.typeVehicule);
  const changeTypeVehicule = (event) => {
    setTypeVehicule(event.target.value);
  }
  const [avionKmC, setAvionKmC] = useState(0);
  const [avionKmM, setAvionKmM] = useState(0);
  const [avionKmL, setAvionKmL] = useState(0);
  const avionCalcul = (event) => {
    let input = event.target.value;
    if (input < 500) {
      setAvionKmC(input);
    } else if (input > 501 && input < 3500) {
      setAvionKmM(input);
    } else {
      setAvionKmL(input);
    }
  };

  //Variable pour le logement
  const [surface, setSurface] = useState(props.surfaceLog === undefined ? 0 : props.surfaceLog);
  const changeSurfValue = (event) => {
    setSurface(event.target.value);
  };
  const [typeLogement, setTypeLogement] = useState("electrique");
  const changeTypeLogement = (event) => {
    setTypeLogement(event.target.value);
  }

  //7 elements
  const [typeElectromenager, setTypeElectromenager] = useState(new Array(eletro.length).fill(true));
  const changeMen = (index) => {
    let newElectro = typeElectromenager;
    newElectro[index] === false ? newElectro[index] = true : newElectro[index] = false;
    setTypeElectromenager(newElectro);
  }
  const [allElectro, setAllElectro] = useState(false);
  const SelectAllElectro = () => {
    allElectro ? setAllElectro(false) : setAllElectro(true);
    typeElectromenager.map((el)=>{
      el = allElectro;
    })
  }

  //8 elements
  const [typeElectronique, setTypeElectronique] = useState(new Array(electrique.length).fill(true));
  const changeElec = (index) => {
    let newElectro = typeElectronique;
    newElectro[index] === false ? newElectro[index] = true : newElectro[index] = false;
    setTypeElectronique(newElectro);
  }

  //Variable pour le régime alimentaire 
  const [typeAlimentation, setTypeAlimentation] = useState("omnivore");
  const alimentSetter = (event) => {
    setTypeAlimentation(event.target.value);
  };

  //Total from service 
  const [totalTransport, setTotaltransport] = useState(["0", "0", "0", "0", "0"]);
  // 
  const [totalLogement, setTotalLogemennt] = useState(["0", "0", "0", "0"]);
  const [totalAll, setTotalAll] = useState(["0", "0"]);

  //Temoins 
  const [temoin, setTemoin] = useState(props.typeTem === undefined ? "etudiant" : props.typeTem);
  const handleTemoin = (event) => {
    setTemoin(event.target.value)
  }

  //Fonctions 
  const setValEl = (bool)=>{
   return bool ? "0" : "1";
  }
  const sendToService = () => {
    // Transport value encapsulation
    //nbKmVelo,nbKmTrain,typeVoiture,nbKmVoiture,nbKmCourt,nbKMoy,nbKmLong
    let transport_value_url = "";
    if (typeVehicule.includes("km")) {
      transport_value_url += typeVehicule + '=' + vehiculeKm
    } else {
      transport_value_url += 'typeVoiture=' + typeVehicule + "&voitureKm=" + vehiculeKm;
    } transport_value_url += '&nbKmCourt=' + avionKmC
      + '&nbKmMoy=' + avionKmM
      + '&nbKmLong=' + avionKmL;
    //Logement value encapsulation
    let log_value_url = '&surface=' + surface
      + '&typeLogement=' + typeLogement
      + '&nbFour=' + setValEl(typeElectromenager[0]) 
        + '&nbLaveV=' + setValEl(typeElectromenager[1])  
          + '&nbLaveL=' + setValEl(typeElectromenager[2])  
            + '&nbSecheL=' + setValEl(typeElectromenager[3])  
              + '&nbRefri=' + setValEl(typeElectromenager[4])  
                + '&nbConge=' + setValEl(typeElectromenager[5])  
                  + '&nbMicroOnde=' + setValEl(typeElectromenager[6])  
                    + '&nbSmartphone=' + setValEl(typeElectronique[0])  
                      + '&nbTv=' + setValEl(typeElectronique[1])  
                        + '&nbTablette=' + setValEl(typeElectronique[2])  
                          + '&nbPcPort=' + setValEl(typeElectronique[3])  
                            + '&nbPcFixe=' + setValEl(typeElectronique[4])  
                              + '&nbConsole=' + setValEl(typeElectronique[5])  
                                + '&nbImprimante=' + setValEl(typeElectronique[6])  
                                  + '&nbBox=' + setValEl(typeElectronique[7])  ;
    // Regime Value
    let reg_value_url = '&typeAlim=' + typeAlimentation;
    //Log it

    console.log(log_value_url);

    //send to service
    axios({
      method: 'get',
      url: 'http://127.0.0.1:5000/carbonCalcul?' + transport_value_url + log_value_url + reg_value_url,
    })
      .then(function (response) {
        console.log(response);
        console.log(response.data);
        console.log(response.data.split(";"));
        let newToTransport = [];
        let newToLogement = [];
        let newTotAll = [];
        response.data.split(";").map((e, i) => {
          if (i < 5) {
            console.log(e, i, "Go trans")
            newToTransport.push(e);
          } else if (i > 4 && i < 9) {
            console.log(e, i, "Go log")
            newToLogement.push(e);
          } else if (i > 8) {
            console.log(e, i, "Go All")
            newTotAll.push(e);
          }
        })
        setTotaltransport(newToTransport);
        setTotalLogemennt(newToLogement);
        setTotalAll(newTotAll);
        console.log(totalTransport, totalLogement, totalAll);
        setTimeout(() => { generateLogForConsommation(); }, 500);
      });
  };

  const generateLogForConsommation = () => {
    console.log(totalLogement, totalTransport, totalAll);
    document.getElementById('totalT').textContent = "Total Transport :  " + totalTransport[4] / 1000 + " kgcO2 \n";
    document.getElementById('totalL').textContent = "Total Logement : " + totalLogement[3] / 1000 + " kgcO2 \n";;
    document.getElementById('totalA').textContent = "Total Alimentation " + totalAll[0] / 1000 + " kgcO2 \n";;
    document.getElementById('totalAll').textContent = " Votre empreinte carbone mensuelle est de " + totalAll[1] + " kgcO2 \n";
    render(
      <BarChart
        axes
        grid
        axisLabels={{ x: 'Type de données', y: 'Consommation en kgCo2' }}
        height={300}
        width={800}
        margin={{ top: 50, right: 100, bottom: 50, left: 100 }}
        barWidth={25}
        data={[
          { x: 'Total Transport', y: totalTransport[4] / 1000, color: 'green' },
          { x: 'Total Logement', y: totalLogement[3] / 1000, color: 'blue' },
          { x: 'Total Alimentation', y: totalAll[0] / 1000, color: 'red' },
          { x: 'Total ', y: totalAll[1], color: 'orange' }
        ]}
      />
      , document.getElementById("graph"));
  }
  const getPrediction = () => {
    if (totalAll[1] === 0) {
      alert("Vous devez d'abord calculer votre empreinte avant de la prédire ")
      return;
    }
    let transport_pred_url = '&totalVelo=' + totalTransport[0] +
      '&totalTrain=' + totalTransport[1] +
      '&totalVoiture=' + totalTransport[2] +
      '&totalAvion=' + totalTransport[3] +
      '&totalTransport=' + totalTransport[4];
    let logement_pred_url = '&totalChauf=' + totalLogement[0]
      + '&totalConsoMen=' + totalLogement[1]
      + '&totalConsoEle=' + totalLogement[2]
      + '&totalConsoLog=' + totalLogement[3];
    let total_pred_url = '&totalAlimConso=' + totalAll[0]
      + '&totalFinal=' + totalAll[1];
    axios({
      method: 'get',
      url: 'http://127.0.0.1:5000/pred?' + transport_pred_url + logement_pred_url + total_pred_url,
    })
      .then(function (response) {
        console.log(response.data);
        document.getElementById('predAnnuel').textContent = " Votre empreinte carbone sur l'année sera de  " + response.data/1000 + " Tonnes de cO2 \n";
      });
  };

  const compareToTem = () => {
    console.log(temoin);
    console.log(profils["profils"]);
    profils["profils"].map((e, i) => {
      if (e.libelle === temoin) {
        render(
          <BarChart
            axes
            grid
            axisLabels={{ x: 'Type de données', y: 'Consommation en kgCo2' }}
            height={300}
            width={800}
            margin={{ top: 50, right: 100, bottom: 50, left: 100 }}
            colorBars
            barWidth={30}
            data={[
              { x: 'Transport', y: totalTransport[4] / 1000, color: 'green' },
              { x: 'Transport Témoin', y: e.value_T / 1000, color: 'green' },
              { x: 'Logement', y: totalLogement[3] / 1000, color: 'blue' },
              { x: 'Logement Témoin', y: e.value_L / 1000, color: 'blue' },
              { x: 'Alimentation', y: totalAll[0] / 1000, color: 'red' },
              { x: 'Alimentation Témoin', y: e.value_A / 1000, color: 'red' },
              { x: 'Total ', y: totalAll[1], color: 'orange' },
              { x: 'Total Témoin', y: e.value_All, color: 'orange' }
            ]}
          />
          , document.getElementById("graph"));
      }
    })
  }

  return (<>
    <div id="transport">
      <h3>Transport</h3>
      <label for="cars">Type de voiture utilisé:</label>

      <select class="w3-select" value={typeVehicule} onChange={changeTypeVehicule} name="cars" id="type_car">
        <option value="essence">Essence</option>
        <option value="diesel">Diesel</option>
        <option value="gpl">GPL</option>
        <option value="electrique">Electrique</option>
        <option value="hybride">Hybride</option>
        <option value="kmVelo">Vélo</option>
        <option value="kmTrain">Train</option>
        <option value="kmBus">Bus</option>
      </select>

      <h5>Entrez le nombre de kilomètres parcourus avec le véhicule utilisé</h5>
      <input class="w3-input" type="number" onChange={kmCalcul} value={vehiculeKm} min="0" name="velo_km" />

      <label>Avion (estimé)</label>
      <input class="w3-input" type="number" onChange={avionCalcul} value={avionKmC} min="0" name="avion_km" />

    </div>
    <div id="logement">

      <h3>Logement</h3>
      <h5>Entrez la surface de votre logement et le type d'énergie utilisé</h5>
      <input class="w3-input" type="number" onChange={changeSurfValue} value={surface} min="0" name="avion_km" />

      <select class="w3-select" value={typeLogement} onChange={changeTypeLogement} name="chaufs" id="type_chauf">
        <option value="electrique">Electrique</option>
        <option value="gaz">Gaz</option>
        <option value="fioul">fioul</option>
      </select>
      <h5>Selectionner vos appareils electromenagers</h5>

      {eletro.map((name, index) => {
        return (
          <div className="toppings-list-item">
            <div className="left-section">
              <input
                type="checkbox"
                id={`custom-checkbox-${index}`}
                name={name}
                value={name}
                onChange={() => changeMen(index)}
              />
              <label htmlFor={`custom-checkbox-${index}`}>{name}</label>
            </div>
          </div>
        );
      })}
      <h5>Selectionner vos appareils electriques</h5>

      {electrique.map((name, index) => {
        console.log(name)
        return (
          <div className="toppings-list-item">
            <div className="left-section">
              <input
                type="checkbox"
                id={`custom-checkbox-${index}`}
                name={name}
                value={name}
                onChange={() => changeElec(index)}
              />
              <label htmlFor={`custom-checkbox-${index}`}>{name}</label>
            </div>
          </div>
        );
      })}
    </div>
    <div id="regime">
      <h3>Consommation</h3>
      <h5>Entrez votre mode d'alimentation</h5>
      <select class="w3-select" onChange={alimentSetter} name="chaufs" id="type_chauf">
        <option value="omnivore">Viande Rouge</option>
        <option value="viande_blanche">Uniquement Viande Blanche</option>
        <option value="vegetarien">Végétarien</option>
      </select>

    </div>
    <div id="result_display">
      <h4 class="w3-blue" id="totalT"></h4>
      <h4 class="w3-blue" id="totalL"></h4>
      <h4 class="w3-blue" id="totalA"></h4>
      <h4 class="w3-blue" id="totalAll"></h4>
      <div id='graph'></div>
      <h4 class="w3-blue" id="predAnnuel"></h4>
      <h4 class="w3-blue" id="conseil"></h4>

    </div>
    <button onClick={sendToService} class="w3-button w3-block">Calculer son empreinte</button>
    <button onClick={getPrediction} class="w3-button w3-block"> Prédire son empreinte</button>
    <h5>Type de profil témoin comparable : </h5>
    <select class="w3-select" onChange={handleTemoin} name="temoin" id="temoins">
      <option value="etudiant">Etudiant</option>
      <option value="commercial">Commercial</option>
      <option value="teletravail">Teletravail</option>
    </select>
    <button onClick={compareToTem} class="w3-button w3-block">Témoin</button>


  </>)
}

export default FormComponent;