import './App.css';
import './w3.css';
import React, { useState } from 'react';
import { conseils } from './config/conseils_config';
import { render } from 'react-dom';

import FormComponent from './FormComponents'
import { profils } from './config/temoin_init_config';
/**
 * To fix :
 *  -> empecher les nombres de passer en négatif : Done
 *  ->  
 * @returns 
 */
function App() {

  const getAdvice = () => {
    let AllConseils = conseils["conseils"];
    document.getElementById("conseil").textContent = "Conseil : " + AllConseils[0].conseil;
    return 'No ';
  }
  const [temoin, setTemoin] = useState("etudiant");
  const handleExemple = (event) => {
    setTemoin(event.target.value)
  }

  const getParam = () => {
    var url = new URL(window.location.href);
    var dist = url.searchParams.get("distance");
    return dist; 
  }

  const [nbKmApp,setnbKmApp] = useState(getParam());
  const [id, setId] = useState("0");
  const handleId = (event) => {
    setId(event.target.value)
  }
  const generateForm = () => {
    profils["profils"].map((e, i) => {
      if (e.libelle === temoin) {
        let nbKm;
        if(nbKmApp != undefined){
          nbKm = nbKmApp;
        }else{
          nbKm=e.nbKm;
        }
    render(
      <FormComponent nbKm={nbKm} typeVehicule={e.typeVehicule} surfaceLog={e.surfaceLog} typeTem={e.libelle} />
      , document.getElementById("formulaire"));
    }});

  }
  return (
    <div className="App">
      <body>
        <h5>Choissisez votre Profil</h5>
        <select class="w3-select" onChange={handleExemple} name="temoin" id="temoins">
          <option value="etudiant">Etudiant</option>
          <option value="commercial">Commercial</option>
          <option value="teletravail">Teletravail</option>
        </select>

        <button onClick={generateForm} class="w3-button w3-block">Appliquer</button>
        <div id='formulaire'></div>
        <button onClick={getAdvice} class="w3-button w3-block">Conseils</button>
      </body>
    </div>
  );
}

export default App;
