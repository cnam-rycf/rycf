class Transport:
    def __init__(self, data ,nbKmVelo,nbKmTrain,typeVoiture,nbKmVoiture,nbKmCourt,nbKMoy,nbKmLong):
        self.dataTransport = data
        self.velo = self.set_conso_velo(nbKmVelo)
        self.consoTrain = self.set_conso_train(nbKmTrain)
        if typeVoiture == "Essence":
            self.consoVoiture = self.set_conso_voiture_essence(nbKmVoiture)
        elif typeVoiture == "Diesel":
           self.consoVoiture = self.set_conso_voiture_diesel(nbKmVoiture)
        elif typeVoiture == "GPL":
            self.consoVoiture = self.set_conso_voiture_GPL(nbKmVoiture)
        elif typeVoiture == "Electrique":
            self.consoVoiture = self.set_conso_voiture_electrique(nbKmVoiture)
        elif typeVoiture == "GNV":
            self.consoVoiture = self.set_conso_voiture_GNV(nbKmVoiture)
        else :
            self.consoVoiture = 0
        self.consoAvion = self.set_trajet_avion_court(nbKmCourt)+self.set_trajet_avion_moyen(nbKMoy)+self.set_trajet_avion_long(nbKmLong)
        self.consoTotal = self.calculateTotal

    def set_conso_velo(self,nbKm):
        return nbKm*self.dataTransport["vehicule"]["velo"]
        
    def set_conso_train(self,nbKm):
        return nbKm*self.dataTransport["vehicule"]["train"]

    def set_conso_voiture_essence(self,nbKm):
        return nbKm*self.dataTransport["vehicule"]["voiture_essence"]

    def set_conso_voiture_diesel(self,nbKm):
        return nbKm*self.dataTransport["vehicule"]["voiture_diesel"]

    def set_conso_voiture_GPL(self,nbKm):
        return nbKm*self.dataTransport["vehicule"]["voiture_GPL"]

    def set_conso_voiture_electrique(self,nbKm):
        return nbKm*self.dataTransport["vehicule"]["voiture_electrique"]

    def set_conso_voiture_GNV(self,nbKm):
        return nbKm*self.dataTransport["vehicule"]["voiture_GNV"]

    def set_conso_voiture_hybride(self,nbKm):
        return nbKm*self.dataTransport["vehicule"]["voiture_hybride"]

    def set_conso_bus(self,nbKm):
        return nbKm*self.dataTransport["vehicule"]["bus"]

    def set_trajet_avion_court(self,nbKm):
        return nbKm*self.dataTransport["avion"]["court"]

    def set_trajet_avion_moyen(self,nbKm):
        return nbKm*self.dataTransport["avion"]["moyen"]

    def set_trajet_avion_long(self,nbKm):
        return nbKm*self.dataTransport["avion"]["long"]

    def calculateTotal(self):
        return self.velo+self.consoTrain+self.consoVoiture+self.consoAvion

    def getTotal(self):
        return self.consoTotal()
    
    def getAllString(self):
        return str(self.velo)+';'+str(self.consoTrain)+';'+str(self.consoVoiture)+';'+str(self.consoAvion)+';'+str(self.calculateTotal())

