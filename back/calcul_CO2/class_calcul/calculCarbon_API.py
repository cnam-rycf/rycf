# -*- coding: utf-8 -*-
"""
@author: peter
"""

# Programme python pour calculer l'empreinte carbone
from course_calcul_class import Course
from logement_calcul_class import Logement
from transport_calcul_class import Transport
import json

import flask
from flask import request, jsonify
app = flask.Flask(__name__)
app.config["DEBUG"] = True

# Recuperation de la configuration des calculs
f = open('../config/config_RYCF.json',)
data = json.load(f)
transport_input_data = data['transport']
logement_input_data = data['logement']
regime_input_data = data['regime_alimentaire']
f.close()

@app.route('/carbonCalcul', methods=['GET'])
def calcul_carbon():
    # Get transport Data
    kmVelo = request.args.get('kmVelo', default = 0, type = int)
    kmTrain = request.args.get('kmTrain', default = 0, type = int)
    typeVoiture = request.args.get('typeVoiture', default = 'Essence', type = str)
    voitureKm = request.args.get('voitureKm', default = 0, type = int)
    nbKmCourt = request.args.get('nbKmCourt', default = 0, type = int)
    nbKmMoy = request.args.get('nbKmMoy', default = 0, type = int)
    nbKmLong = request.args.get('nbKmLong', default = 0, type = int)
    t1 = Transport(transport_input_data,kmVelo,kmTrain,typeVoiture,voitureKm,nbKmCourt,nbKmMoy,nbKmLong)
    # Get Logement 
    surface = request.args.get('surface', default = 0, type = int)
    typeLogement = request.args.get('typeLogement', default = 'electrique', type = str)
    nbFour = request.args.get('nbFour', default = 0, type = int)
    nbLaveV = request.args.get('nbLaveV', default = 0, type = int)
    nbLaveL = request.args.get('nbLaveL', default = 0, type = int)
    nbSecheL = request.args.get('nbSecheL', default = 0, type = int)
    nbRefri = request.args.get('nbRefri', default = 0, type = int)
    nbConge = request.args.get('nbConge', default = 0, type = int)
    nbMicroOnde = request.args.get('nbMicroOnde', default = 0, type = int)
    nbSmartphone = request.args.get('nbSmartphone', default = 0, type = int)
    nbTv = request.args.get('nbTv', default = 0, type = int)
    nbTablette = request.args.get('nbTablette', default = 0, type = int)
    nbPcPort = request.args.get('nbPcPort', default = 0, type = int)
    nbPcFixe = request.args.get('nbPcFixe', default = 0, type = int)
    nbConsole = request.args.get('nbConsole', default = 0, type = int)
    nbImprimante = request.args.get('nbImprimante', default = 0, type = int)
    nbBox = request.args.get('nbBox', default = 0, type = int)
    typeAlim = request.args.get('typeAlim', default = 'Omnivore', type = str)

    donnees_app_ele = [{
        'libelle':"smartphone",
        'nb':nbSmartphone,
    },{
        'libelle':"tv",
        'nb':nbTv,
    },{
        'libelle':"tablette",
        'nb':nbTablette,
    },{
        'libelle':"ordinateur_portable",
        'nb':nbPcPort,
    },{
        'libelle':"ordinateur_fixe",
        'nb':nbPcFixe,
    },{
        'libelle':"console",
        'nb':nbConsole,
    },{
        'libelle':"imprimante",
        'nb':nbImprimante,
    },{
        'libelle':"box",
        'nb':nbBox,
    }]
    donnees_app_men = [{
        'libelle':"four",
        'nb':nbFour,
    },{
        'libelle':"lave-vaisselle",
        'nb': nbLaveV,
    },{
        'libelle':"lave_linge",
        'nb':nbLaveL,
    },{
        'libelle':"seche_linge",
        'nb':nbSecheL,
    },{
        'libelle':"refrigerateur",
        'nb':nbRefri,
    },{
        'libelle':"congelateur",
        'nb':nbConge,
    },{
        'libelle':"micro-onde",
        'nb':nbMicroOnde,
    }]
    l1 = Logement(logement_input_data,surface,typeLogement,donnees_app_men,donnees_app_ele)
    c1 = Course(regime_input_data,typeAlim)
    totalCO2 = (t1.getTotal() +l1.getTotal() + c1.getTotal())/1000
    strCourse = str(c1.getTotal())
    strToReturn= t1.getAllString()+";"+l1.getAllString()+";"+strCourse+";"+str(totalCO2)
    return strToReturn


@app.route('/pred', methods=['GET'])
def pred():
    totalVelo=request.args.get('totalVelo', default = 0, type = int)
    totalTrain=request.args.get('totalTrain', default = 0, type = int)
    totalVoiture=request.args.get('totalVoiture', default = 0, type     = int)
    totalAvion=request.args.get('totalAvion', default = 0, type = int)
    totalTransport=request.args.get('totalTransport', default = 0, type = int)
    totalChauf=request.args.get('totalChauf', default = 0, type = int)
    totalConsoMen=request.args.get('totalConsoMen', default = 0, type = int)
    totalConsoEle=request.args.get('totalConsoEle', default = 0, type = int)
    totalConsoLog=request.args.get('totalConsoLog', default = 0, type = int)
    totalAlimConso=request.args.get('totalAlimConso', default = 0, type = int)
    totalFinal = request.args.get('totalFinal', default = 0, type = float)
    print(totalFinal)
    return str(totalFinal*9+totalFinal*1.06*3)

app.run()