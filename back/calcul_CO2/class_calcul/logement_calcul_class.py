class Logement:
    def __init__(self,data,surface,type_chauffage,type_app_men,type_app_ele):
        self.dataLogement = data
        self.consoChauf = self.set_consommation(surface,type_chauffage)
        self.consoMen = self.set_consommation_app_men(type_app_men)
        self.consoEle = self.set_consommation_app_ele(type_app_ele)
        self.consoTotal = self.calculateTotal

    def set_consommation(self,surface,typeChauffage):
        return surface * self.dataLogement["chauffage"][typeChauffage]

    def set_consommation_app_men(self,type_app):
        conso_app_men = 0
        for app_men in type_app:
            conso_app_men = (self.dataLogement["type_appareils_menagers"][app_men['libelle']]*app_men['nb'])/self.dataLogement["duree_appareils_menagers"][app_men['libelle']]
        return conso_app_men
    
    def set_consommation_app_ele(self,type_app):
        conso_app_ele = 0
        for app_ele in type_app:
            conso_app_ele = (self.dataLogement["type_appareils_electroniques"][app_ele['libelle']]*app_ele['nb'])/self.dataLogement["duree_appareils_electroniques"][app_ele['libelle']]
        return conso_app_ele

    def calculateTotal(self):
        return self.consoChauf + self.consoMen + self.consoEle
        
    def getTotal(self):
        return self.consoTotal()

    def getAllString(self):
        return str(self.consoChauf)+';'+ str(self.consoMen)+';'+str(self.consoEle)+';'+str(self.calculateTotal())
