class Course :
    def __init__(self,donnee_al,regime_alm):
        self.dataAlimentaire = donnee_al
        self.consoCourse = self.set_conso_course(regime_alm)
        
    def set_conso_course(self,regime_alm):
        if regime_alm == "vegetarien" :
            return self.dataAlimentaire["vegetarien"]
        elif regime_alm == "viande_blanche":
            return self.dataAlimentaire["viande_blanche"]
        else :
            return self.dataAlimentaire["omnivore"]

    def getTotal(self):
        return self.consoCourse