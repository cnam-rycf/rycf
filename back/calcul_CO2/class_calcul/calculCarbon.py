# -*- coding: utf-8 -*-
"""
Created on Fri Jul 16 14:41:07 2021

Simulateur de calcul par programme python 
@author: peter
"""# Programme python pour calculer l'empreinte carbone
from course_calcul_class import Course
from logement_calcul_class import Logement
from transport_calcul_class import Transport
import json
# Opening JSON file
f = open('../config/config_RYCF.json',)
  
# returns JSON object as 
# a dictionary
data = json.load(f)
  
transport_input_data = data['transport']
logement_input_data = data['logement']
regime_input_data = data['regime_alimentaire']

# Closing file
f.close()

# Simulation input electromenager & electroniques
simulation_donnees_app_ele = [{
    'libelle':"smartphone",
    'nb':1
},
{
    'libelle':"tv",
    'nb':1
},
{
    'libelle':"tablette",
    'nb':1
},{
    'libelle':"ordinateur_portable",
    'nb':1
},{
    'libelle':"ordinateur_fixe",
    'nb':1
},{
    'libelle':"console",
    'nb':1
},{
    'libelle':"imprimante",
    'nb':1
},{
    'libelle':"box",
    'nb':1
}]
simulation_donnees_app_men = [{
    'libelle':"four",
    'nb':1,
},
{
    'libelle':"lave-vaisselle",
    'nb':1,
},
{
    'libelle':"lave_linge",
    'nb':1,
},{
    'libelle':"seche_linge",
    'nb':1,
},{
    'libelle':"refrigerateur",
    'nb':1,
},{
    'libelle':"congelateur",
    'nb':1,
},{
    'libelle':"micro-onde",
    'nb':1,
}]


t1 = Transport(transport_input_data,0,0,"Essence",0,0,0,0)
l1 = Logement(logement_input_data,40,"electrique",simulation_donnees_app_men,simulation_donnees_app_ele)
c1 = Course(regime_input_data,"Vegetarien")

totalCO2 = (t1.getTotal() +l1.getTotal() + c1.getTotal())/1000
print("Donnees de consommation Transport : ",t1.getTotal()," gCO2")
print("Donnees de consommation logement : ",l1.getTotal()," gCO2")
print("Donnees de consommation Course : ",c1.getTotal()," gC02")
print("Données de consommation Total : ",totalCO2," kgCO2")
