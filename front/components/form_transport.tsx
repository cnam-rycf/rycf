import { Text, View, TextInput, Button, Alert, StyleSheet } from "react-native";
import React from "react";
import { useForm, Controller } from "react-hook-form";
import Constants from 'expo-constants';

import { useState } from "react";
import SelectPicker from 'react-native-form-select-picker/src/index'; 

       /**
         <SelectPicker
        onValueChange={ (value) => {
            setVoitureType(value);
        }}
        selected={voitureType}
        >	
        {Object.values(options).map((val, index) => (
            <SelectPicker.Item label={val} value={val} key={index} />
        ))}
    </SelectPicker>*/

const options = ["Essence", "Diesel", "GPL","Electrique","GNV","Hybride"];

/**
 * Composant du formulaire d'entrées de données : Partie Transport
 * Formulaire :
 * 
 * 
 * @returns 
 */
export function FormTransport() {
    const { control, handleSubmit, formState: { errors } } = useForm();
    const onSubmit = data => console.log(data);
    const [veloKm,setVeloKm] = useState("0");
    const [trainKm,setTrainKm] = useState("0");
    const [voitureKm,setVoitureKm] = useState("0");
    const [busKm,setBusKm] = useState("0");
    const [voitureType,setVoitureType] = useState();

    return (<>
        <Text style={styles.label}>Nombre de kilomètres à Vélo par semaine (Estimé)</Text>
        <Controller
            control={control}
            render={({ field: { onChange, onBlur, value } }) => (
                <TextInput
                    onBlur={onBlur}
                    onChangeText={value => value.match(/[0-9]*/gm) && setVeloKm(value)}
                    value={veloKm}
                />
            )}
            name="velo"
            rules={{ required: true }}
            defaultValue="0"
        />
        {errors.velo && <Text>This is required.</Text>}
        <Text style={styles.label}>Nombre de kilomètres à Train par semaine (Estimé)</Text>
        <Controller
            control={control}
            render={({ field: { onChange, onBlur, value } }) => (
                <TextInput
                    onBlur={onBlur}
                    onChangeText={value => value.match(/[0-9]*/gm) && setTrainKm(value)}
                    value={trainKm}
                />
            )}
            name="train"
            defaultValue="0"
        />
        {errors.train && <Text>This is required.</Text>}

        <Text style={styles.label}>Nombre de kilomètres à Bus par semaine (Estimé)</Text>

        <Controller
            control={control}
            render={({ field: { onChange, onBlur, value } }) => (
                <TextInput
                    onBlur={onBlur}
                    onChangeText={value => value.match(/[0-9]*/gm) && setBusKm(value)}
                    value={busKm}
                />
            )}
            name="bus"
            defaultValue="0"
        />
        {errors.bus && <Text>This is required.</Text>}
        
        <SelectPicker
        onValueChange={ (value) => {
            setVoitureType(value);
        }}
        selected={voitureType}
        >	
        {Object.values(options).map((val, index) => (
            <SelectPicker.Item label={val} value={val} key={index} />
        ))}
    </SelectPicker>
        <Text style={styles.label}>Nombre de kilomètres à Voiture par semaine (Estimé)</Text>
        <Controller
            control={control}
            render={({ field: { onChange, onBlur, value } }) => (
                <TextInput
                    onBlur={onBlur}
                    onChangeText={value => value.match(/[0-9]*/gm) && setVoitureKm(value)}
                    value={voitureKm}
                />
            )}
            name="voiture"
            defaultValue="0"
        />
        {errors.voiture && <Text>This is required.</Text>}

        <Button title="Submit" onPress={handleSubmit(onSubmit)} />
    </>
    );
}

const styles = StyleSheet.create({
    label: {
      color: 'white',
      margin: 20,
      marginLeft: 0,
    },
    button: {
      marginTop: 40,
      color: 'white',
      height: 40,
      backgroundColor: '#ec5990',
      borderRadius: 4,
    },
    container: {
      flex: 1,
      justifyContent: 'center',
      paddingTop: Constants.statusBarHeight,
      padding: 8,
      backgroundColor: '#0e101c',
    },
    input: {
      backgroundColor: 'white',
      borderColor: 'none',
      height: 40,
      padding: 10,
      borderRadius: 4,
    },
  });
  //SDK Location : C:\Users\peter\AppData\Local\Android\Sdk