import { Text, View, TextInput, Button, Alert } from "react-native";
import React from "react";
import { useForm, Controller } from "react-hook-form";
import { FormTransport } from "./form_transport";

/**
 * Composant du formulaire d'entrées de données
 * Formulaire :
 *  => Partie Transport 
 *      = Choix d'utiliser Maps () 
 *          => Si oui : Chercher le fichier résultat de Maps
 *          => Si non : Appeller le composant d'évaluation du 
 *  => Partie Logement
 *          
 *  =>
 * 
 * @returns 
 */
export function FormCarbon() {
    const { control, handleSubmit, formState: { errors } } = useForm();
    const onSubmit = data => console.log(data);

    return (
        <View>
            <FormTransport />
            <Button title="Submit" onPress={handleSubmit(onSubmit)} />
        </View>
    );
}
